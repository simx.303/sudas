import { supabase } from "$lib/supabaseClient";

export async function load() {
  const { data } = await supabase.auth.getUser();
  return {
    posts: data ?? [],
  };
}