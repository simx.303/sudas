import { supabase } from "$lib/supabaseClient";
import { vardasKonvert } from "$lib/vienas";

export async function load() {

  const {data,error} = await supabase.rpc('naujausi');
  if (error) console.error(error)
  else {
    return {
      posts: data ?? [],
    }
  }
}